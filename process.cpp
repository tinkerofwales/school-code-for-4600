/*
Author: Jeremy Tinker
	class definition for problem 3, homework 2, for CSCE 4600
*/
#include "process.h"
process::process(int i, int m, int c)
{
	cyc=c;
	mem=m;
	pid=i;
}
int process::cycles(){return cyc;}
int process::memory(){return mem;}
int process::id(){return pid;}
