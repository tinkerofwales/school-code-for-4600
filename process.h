/*
Author: Jeremy Tinker
        class declarations for problem 3, homework 2, for CSCE 4600
*/
#include <iostream>
#include <random>

using namespace std;
class process
{
	friend class process_factory;
	private:
		int cyc;
		int mem;
		int pid;
	protected:
		process(int, int, int);//id, memory, cycles
	public:
		int cycles();
		int memory();
		int id();
};

class process_factory
{
	private:
		default_random_engine gen;
		lognormal_distribution<double> *cycle_dist;
		lognormal_distribution<double> *mem_dist;
		int cycle_mean;
		int memory_mean;
		int curr_pid;
	public:
		process_factory();
		~process_factory();
		process gen_proc();
		void print_means();
};
