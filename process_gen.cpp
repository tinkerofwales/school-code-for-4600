/*
Author: Jeremy Tinker
        class definition for problem 3, homework 2, for CSCE 4600
*/
#include "process.h"
#include <iostream>
#include <chrono>

using namespace std;

process_factory::process_factory()
{
	unsigned sd=chrono::system_clock::now().time_since_epoch().count();
	gen.seed(sd);
	cycle_dist= new lognormal_distribution <double> (8.199,1);
	mem_dist= new lognormal_distribution <double> (2.23,1);
	cycle_mean=0;
	memory_mean=0;
	curr_pid=0;
}
process_factory::~process_factory()
{
	delete cycle_dist;
	delete mem_dist;
}
process process_factory::gen_proc()
{
	int proc_mem=(int)(*mem_dist)(gen)%100+1;
	if(proc_mem<0) proc_mem=-proc_mem;

	int proc_cyc=(int)(*cycle_dist)(gen);
	if(proc_cyc<0) proc_cyc=-proc_cyc;
	while(proc_cyc>1000 && proc_cyc<11000)
	{
		proc_cyc=(int)(*cycle_dist)(gen);
		if(proc_cyc<0) proc_cyc=-proc_cyc;
	}

	if(cycle_mean==0) cycle_mean=proc_cyc;
	else cycle_mean=(cycle_mean+proc_cyc)/2;
	if(memory_mean==0) memory_mean=proc_mem;
	else memory_mean=(memory_mean+proc_mem)/2;

	curr_pid++;

	process ret(curr_pid, proc_mem, proc_cyc);
	return ret;
}

void process_factory::print_means()
{
	cout << "Mean memory allocation: " <<memory_mean << "kB" <<endl;
	cout << "Mean cycle count: " << cycle_mean << " cycles" << endl;
}
