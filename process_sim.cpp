/*
Author: Jeremy Tinker
	Main and ancilliary function for problem 3, homework 2, for CSCE 4600
*/

#include <iostream>
#include "process.h"
#include <vector>

using namespace std;
void print_procs(vector<process>, int);
int main()
{
	process_factory proc_fac;
	vector<process> processes;
	int procs;
	char tap;

	cout << "Enter the number of processes to generate: ";
	cin >> procs;

	while(processes.size()<procs)
	{
		processes.emplace_back(proc_fac.gen_proc());
//		cout << processes[processes.size()-1].id();
//		cout << " ";
//		cout << processes[processes.size()-1].memory() << endl;
	}
	cout << procs << " processes generated" << endl;
	proc_fac.print_means();
	cout << "how many processes to display: ";
	cin >> procs;
	print_procs(processes, procs);
}

void print_procs(vector<process> procs, int p)
{
	cout << "PID\tkB\tCycles" << endl;
	while(p-1>=0)
	{
		cout << procs[p-1].id()<<"\t"<< procs[p-1].memory();
		cout << "\t"<< procs[p-1].cycles() << endl;
		p--;
	}
}
